package com.elmahalwy.weathertask.base

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.TimePicker
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.viewbinding.ViewBinding
import com.elmahalwy.weathertask.R
import com.elmahalwy.weathertask.network.RemoteDataSource
import java.util.*

abstract class BaseFragment<DB : ViewBinding, VM : ViewModel, BR : BaseRepository> : Fragment() {
    protected lateinit var binding: DB
    protected lateinit var viewModel: VM
    protected val remoteDataSource = RemoteDataSource()
    protected lateinit var userPreferences: UserPreferences
    protected lateinit var navController: NavController




    private var v_noInternet: View? = null
    private var v_loading: View? = null
    private var progress_bar: ProgressBar? = null
    private var vEmpty: View? = null
    private var v_serverError: View? = null
    private var tvError: TextView? = null
    private var btn_retry: Button? = null
    private var loadingDialog: Dialog? = null
    var shake: Animation? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        userPreferences = UserPreferences(requireContext())
        retainInstance = true
        binding = getFragmentBinding(inflater, container)
        val factory = ViewModelFactory(getFragmentRepository())
        viewModel = ViewModelProvider(this, factory).get(getViewModel())

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.let {
            try {
                navController = Navigation.findNavController(it)
            } catch (e: java.lang.Exception) {
            }
        }

    }

    abstract fun getFragmentBinding(inflater: LayoutInflater, container: ViewGroup?): DB
    abstract fun getViewModel(): Class<VM>
    abstract fun getFragmentRepository(): BR



    fun showMainLoading() {
        v_loading?.visibility = View.VISIBLE
    }

    fun hideMainLoading() {
        v_loading?.visibility = View.GONE
    }

    fun showLoadMoreProgress() {
        progress_bar?.visibility = View.VISIBLE
    }

    fun hideLoadMoreProgress() {
        progress_bar?.visibility = View.GONE
    }

    fun showSubLoading() {
        if (loadingDialog == null) {
            loadingDialog = Dialog(requireActivity(), R.style.NewDialog1)
            loadingDialog!!.setContentView(R.layout.view_dialog_loading)
            loadingDialog!!.setCancelable(false)
        }
        loadingDialog!!.show()
    }

    fun hideSubLoading() {
        loadingDialog?.dismiss()
    }

    fun openWeb(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }

    fun pickTimeDialog(textFieldTime: TextView) {
        val mTimePicker: TimePickerDialog
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute = mcurrentTime.get(Calendar.MINUTE)

        mTimePicker =
            TimePickerDialog(requireContext(), object : TimePickerDialog.OnTimeSetListener {
                override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
                    val AM_PM: String
                    Log.e("minutes", minute.toString())
                    AM_PM = if (hourOfDay < 12) {
                        "am"
                    } else {
                        "pm"
                    }

                    textFieldTime.setText(
                        String.format(
                            Locale.ENGLISH,
                            "%02d:%02d",
                            if (hourOfDay === 12 || hourOfDay === 0) 12 else hourOfDay % 12,
                            minute
                        ) + " " + AM_PM
                    )
                    textFieldTime.error = null
//                    Log.e("hour", convertToHour(textFieldTime.text.toString())!!)

                }
            }, hour, minute, false)

        textFieldTime.setOnClickListener {
            mTimePicker.show()
        }


    }

    fun NavController.safeNavigate(direction: NavDirections) {
        currentDestination?.getAction(direction.actionId)?.run { navigate(direction) }
    }

    fun NavController.safeNavigate(
        @IdRes currentDestinationId: Int,
        @IdRes id: Int,
        args: Bundle? = null
    ) {
        if (currentDestinationId == currentDestination?.id) {
            navigate(id, args)
        }
    }


}
