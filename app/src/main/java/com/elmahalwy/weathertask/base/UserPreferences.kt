package com.elmahalwy.weathertask.base


import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.preferences.Preferences
import androidx.datastore.preferences.*

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class UserPreferences(context: Context) {

    private val applicationContext = context.applicationContext
    private val dataStore: DataStore<Preferences> = applicationContext.createDataStore(
        name = "WeatherTaskDataStore",
    )
    suspend fun saveLanguage(language: String) {
        dataStore.edit { preferences ->
            preferences[KEY_LANGUAGE] = language
        }
    }

    val getLanguage: Flow<String?>
        get() = dataStore.data.map { preferences -> preferences[KEY_LANGUAGE] ?: "en" }

    companion object {
        private val KEY_LANGUAGE = preferencesKey<String>("cookery_language")
    }
}
