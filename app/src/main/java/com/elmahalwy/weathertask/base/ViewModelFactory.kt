package com.elmahalwy.weathertask.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.elmahalwy.weathertask.ui.main.MainReposeitry
import com.elmahalwy.weathertask.ui.main.MainViewModel


@Suppress("UNCHECKED_CAST")
class ViewModelFactory
    (private val repository: BaseRepository) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(MainViewModel::class.java) ->
                MainViewModel(repository as MainReposeitry) as T

            else -> throw IllegalArgumentException("View Model Class not found ")
        }
    }
}
