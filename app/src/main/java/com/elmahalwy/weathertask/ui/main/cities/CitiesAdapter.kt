package com.elmahalwy.weathertask.ui.main.cities

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.elmahalwy.weathertask.databinding.ItemCityLayoutBinding
import com.elmahalwy.weathertask.models.CityModel

import java.util.*

class CitiesAdapter<T>(
    private val mItemsList: ArrayList<CityModel>,
    private val mItemClickListener: (View, Int) -> Unit
) : RecyclerView.Adapter<CitiesAdapter.CitiesViewHolder<T>>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CitiesAdapter.CitiesViewHolder<T> {
        val layoutInflater = LayoutInflater.from(parent.context);
        val binding = ItemCityLayoutBinding.inflate(layoutInflater, parent, false)
        return CitiesViewHolder(binding, mItemClickListener)
    }

    override fun onBindViewHolder(holder: CitiesViewHolder<T>, position: Int) {
        holder.bind(mItemsList[position])

    }

    override fun getItemCount(): Int = mItemsList.size

    fun getItems(): List<CityModel> = mItemsList

    fun deletetItem(position: Int) {
        mItemsList.removeAt(position)
        notifyItemRemoved(position)
        notifyDataSetChanged()
    }

    fun addMoreItems(items: List<CityModel>) {
        var startPosition = 0
        var endPosition = mItemsList.size + items.size

        if (mItemsList.size != 0) {
            startPosition = mItemsList.size
        }

        mItemsList.addAll(items)
        notifyItemRangeInserted(startPosition, endPosition)
    }

    fun setItems(items: List<CityModel>) {
        mItemsList.clear()
        mItemsList.addAll(items)
        notifyDataSetChanged()
    }

    fun addItem(item: CityModel) {
        mItemsList.add(0, item)
        notifyDataSetChanged()
    }


    class CitiesViewHolder<T>(
        var itemBinding: ItemCityLayoutBinding,
        mItemClickListener: (View, Int) -> Unit
    ) :
        RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.ivInfo.setOnClickListener {
                mItemClickListener(it, adapterPosition)
            }

            itemBinding.container.setOnClickListener {
                mItemClickListener(it, adapterPosition)
            }

        }

        fun bind(city: CityModel) {
            itemBinding.tvCityName.text = city.name
        }
    }


}
