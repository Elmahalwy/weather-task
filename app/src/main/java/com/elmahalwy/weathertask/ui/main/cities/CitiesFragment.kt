package com.elmahalwy.weathertask.ui.main.cities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elmahalwy.weathertask.R
import com.elmahalwy.weathertask.base.BaseFragment
import com.elmahalwy.weathertask.databinding.FragmentCitiesBinding
import com.elmahalwy.weathertask.models.CityModel
import com.elmahalwy.weathertask.network.Api
import com.elmahalwy.weathertask.ui.main.MainReposeitry
import com.elmahalwy.weathertask.ui.main.MainViewModel


class CitiesFragment : BaseFragment<FragmentCitiesBinding, MainViewModel, MainReposeitry>() {

    lateinit var citiesAdapter: CitiesAdapter<Object>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initCities()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.tvTitle.text = getString(R.string.cities)
        binding.toolbar.ivBack.visibility = View.GONE
        setupCities()
        setCities()
        handleClick()
    }

    /** Cities RV */
    private fun initCities() {
        citiesAdapter = CitiesAdapter(arrayListOf()) { view, position ->
            onCityItemClick(view, position)
        }

    }

    private fun setupCities() {
        binding.rvCities.apply {
            val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            layoutManager = linearLayoutManager
            setHasFixedSize(true)
            adapter = citiesAdapter

        }


    }

    private fun onCityItemClick(view: View, position: Int) {
        when (view.id) {
            R.id.container -> {
                val args = Bundle()
                args.putString("city_name", citiesAdapter.getItems()[position].name)
                navController.safeNavigate(
                    R.id.citiesFragment,
                    R.id.action_citiesFragment_to_cityInformationFragment,
                    args
                )
            }

            R.id.ivInfo -> {
                val args = Bundle()
                args.putString("city_name", citiesAdapter.getItems()[position].name)
                navController.safeNavigate(
                    R.id.citiesFragment,
                    R.id.action_citiesFragment_to_cityHistoricalFragment, args
                )

            }

        }
    }

    fun setCities() {
        val list: ArrayList<CityModel> = ArrayList()
        list.add(CityModel("London", 0.0, 0.0))
        list.add(CityModel("Vienna", 0.0, 0.0))
        list.add(CityModel("Paris", 0.0, 0.0))
        citiesAdapter.setItems(list)
    }

    /** End Cities  RV */


    fun handleClick() {
        binding.btnAddCity.setOnClickListener {
            navController.safeNavigate(
                R.id.citiesFragment,
                R.id.action_citiesFragment_to_addCityBottomSheetFragment
            )
        }
    }

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentCitiesBinding.inflate(inflater, container, false)

    override fun getViewModel() = MainViewModel::class.java

    override fun getFragmentRepository(): MainReposeitry {
        val api = remoteDataSource.buildApi(Api::class.java)
        return MainReposeitry(api)
    }
}
