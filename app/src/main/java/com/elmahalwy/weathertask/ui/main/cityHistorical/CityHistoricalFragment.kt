package com.elmahalwy.weathertask.ui.main.cityHistorical

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elmahalwy.weathertask.base.BaseFragment
import com.elmahalwy.weathertask.databinding.FragmentCityHistoricalBinding
import com.elmahalwy.weathertask.models.CityHistoricalModel
import com.elmahalwy.weathertask.network.Api
import com.elmahalwy.weathertask.ui.main.MainReposeitry
import com.elmahalwy.weathertask.ui.main.MainViewModel
import com.elmahalwy.weathertask.utils.SharedPrefencesManager


class CityHistoricalFragment :
    BaseFragment<FragmentCityHistoricalBinding, MainViewModel, MainReposeitry>() {
    lateinit var cityHistoricalAdapter: CityHistoricalAdapter<Object>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initCityHistorical()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.tvTitle.text = requireArguments().getString("city_name")!!
        setupCityHistorical()
        setCities()
        handleClick()
    }


    fun handleClick() {
        binding.toolbar.ivBack.setOnClickListener {
            navController.navigateUp()
        }
    }


    /** City Historical RV */
    private fun initCityHistorical() {
        cityHistoricalAdapter = CityHistoricalAdapter(arrayListOf()) { view, position ->
        }

    }

    private fun setupCityHistorical() {
        binding.rvCityHistorical.apply {
            val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            layoutManager = linearLayoutManager
            setHasFixedSize(true)
            adapter = cityHistoricalAdapter

        }

    }


    fun setCities() {
        if (SharedPrefencesManager.getCityHistorical(requireContext()) != null) {
            binding.emptyView.emptyContainer.visibility = View.GONE
            val historicalList: ArrayList<CityHistoricalModel> = ArrayList()
            historicalList.addAll(
                SharedPrefencesManager.getCityHistorical(requireContext())!!
                    .filter { it.cityName == arguments?.get("city_name") })
            cityHistoricalAdapter.setItems(historicalList)
        } else {
            binding.emptyView.emptyContainer.visibility = View.VISIBLE
        }
    }

    /** End City Historical RV */

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentCityHistoricalBinding.inflate(inflater, container, false)

    override fun getViewModel() = MainViewModel::class.java

    override fun getFragmentRepository(): MainReposeitry {
        val api = remoteDataSource.buildApi(Api::class.java)
        return MainReposeitry(api)
    }
}
