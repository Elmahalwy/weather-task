package com.elmahalwy.weathertask.ui.main.cityHistorical

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.elmahalwy.weathertask.databinding.ItemCityHistoricalLayoutBinding
import com.elmahalwy.weathertask.databinding.ItemCityLayoutBinding
import com.elmahalwy.weathertask.models.CityHistoricalModel
import com.elmahalwy.weathertask.models.CityModel

import java.util.*

class CityHistoricalAdapter<T>(
    private val mItemsList: ArrayList<CityHistoricalModel>,
    private val mItemClickListener: (View, Int) -> Unit
) : RecyclerView.Adapter<CityHistoricalAdapter.CityHistoricalViewHolder<T>>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CityHistoricalAdapter.CityHistoricalViewHolder<T> {
        val layoutInflater = LayoutInflater.from(parent.context);
        val binding = ItemCityHistoricalLayoutBinding.inflate(layoutInflater, parent, false)
        return CityHistoricalViewHolder(binding, mItemClickListener)
    }

    override fun onBindViewHolder(holder: CityHistoricalViewHolder<T>, position: Int) {
        holder.bind(mItemsList[position])

    }

    override fun getItemCount(): Int = mItemsList.size

    fun getItems(): List<CityHistoricalModel> = mItemsList


    fun setItems(items: List<CityHistoricalModel>) {
        mItemsList.clear()
        mItemsList.addAll(items)
        notifyDataSetChanged()
    }


    class CityHistoricalViewHolder<T>(
        var itemBinding: ItemCityHistoricalLayoutBinding,
        mItemClickListener: (View, Int) -> Unit
    ) :
        RecyclerView.ViewHolder(itemBinding.root) {

        init {


        }

        fun bind(city: CityHistoricalModel) {
            itemBinding.tvDate.text = city.date
            itemBinding.tvWeather.text = city.weatherStatus
        }
    }


}
