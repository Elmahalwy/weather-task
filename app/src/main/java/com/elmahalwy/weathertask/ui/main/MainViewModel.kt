package com.elmahalwy.weathertask.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.elmahalwy.weathertask.models.CityInformationResponse
import com.elmahalwy.weathertask.network.Resource
import kotlinx.coroutines.launch

class MainViewModel(val repository: MainReposeitry) : ViewModel() {

    private val _responseGetCityInformation: MutableLiveData<Resource<CityInformationResponse>> =
        MutableLiveData()
    val responseGetCityInformation: LiveData<Resource<CityInformationResponse>> get() = _responseGetCityInformation
    fun getCityInformation(searchText: String) =
        viewModelScope.launch {
            _responseGetCityInformation.value = Resource.Loading
            _responseGetCityInformation.value = repository.getCityInformation(searchText)
        }
}
