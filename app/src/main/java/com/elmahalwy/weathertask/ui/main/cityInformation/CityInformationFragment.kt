package com.elmahalwy.weathertask.ui.main.cityInformation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.elmahalwy.weathertask.R
import com.elmahalwy.weathertask.base.BaseFragment
import com.elmahalwy.weathertask.base.CommonKeys
import com.elmahalwy.weathertask.base.convertKelivnToClieus
import com.elmahalwy.weathertask.base.getCurrentDate
import com.elmahalwy.weathertask.databinding.FragmentCityInformationBinding
import com.elmahalwy.weathertask.models.CityHistoricalModel
import com.elmahalwy.weathertask.network.Api
import com.elmahalwy.weathertask.network.RemoteDataSource
import com.elmahalwy.weathertask.network.Resource
import com.elmahalwy.weathertask.ui.main.MainReposeitry
import com.elmahalwy.weathertask.ui.main.MainViewModel
import com.elmahalwy.weathertask.utils.SharedPrefencesManager
import java.util.*


class CityInformationFragment :
    BaseFragment<FragmentCityInformationBinding, MainViewModel, MainReposeitry>() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.tvTitle.visibility = View.GONE
        binding.tvCityName.text = requireArguments().getString("city_name")!!
        getCityInformation(requireArguments().getString("city_name")!!)
        handleClick()

    }

    fun handleClick() {
        binding.toolbar.ivBack.setOnClickListener {
            navController.navigateUp()
        }
    }


    /** Get Weather  City Information From End Point */
    fun getCityInformation(searchText: String) {
        viewModel.getCityInformation(searchText)
        viewModel.responseGetCityInformation.observe(viewLifecycleOwner) {
            showSubLoading()
            when (it) {
                is Resource.Success -> {
                    hideSubLoading()
                    if (it.value.cod == CommonKeys.STATUS_SUCCESS) {

                        if (!it.value.weather.isNullOrEmpty()) {
                            binding.tvDescription.text = it.value.weather[0].description
                            Glide.with(requireContext())
                                .load(RemoteDataSource.BASE_URL + "img/w/" + it.value.weather[0].icon)
                                .into(binding.ivCityWeather)

                            saveData(
                                CityHistoricalModel(
                                    searchText,
                                    getCurrentDate(),
                                    it.value.weather[0].description
                                )
                            )
                        } else {
                            binding.tvDescription.visibility = View.GONE
                            binding.tvDescriptionConstant.visibility = View.GONE
                        }
                        binding.tvTemperature.text =
                            String.format(
                                Locale.ENGLISH,
                                "%.2f",
                                convertKelivnToClieus(it.value.main.temp)
                            ) + " C"
                        binding.tvHumidity.text = it.value.main.humidity.toString() + "%"
                        binding.tvWindowSpeed.text = it.value.wind.speed.toString()
                    } else {
                        Toast.makeText(requireContext(), it.value.message, Toast.LENGTH_LONG)
                            .show()

                    }
                }
                is Resource.Failure -> {
                    hideSubLoading()
                    Toast.makeText(requireContext(), getString(R.string.error), Toast.LENGTH_LONG)
                        .show()
                }
            }
        }

    }

    /** Save Data Of City Hsitoircal */
    fun saveData(cityHistoricalModel: CityHistoricalModel) {
        val cityHistoicalList: ArrayList<CityHistoricalModel> = ArrayList()
        if (SharedPrefencesManager.getCityHistorical(requireContext()) != null)
            cityHistoicalList.addAll(SharedPrefencesManager.getCityHistorical(requireContext())!!)
        cityHistoicalList.add(cityHistoricalModel)
        SharedPrefencesManager.setCityHistorical(cityHistoicalList, requireContext())
    }

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentCityInformationBinding.inflate(inflater, container, false)

    override fun getViewModel() = MainViewModel::class.java

    override fun getFragmentRepository(): MainReposeitry {
        val api = remoteDataSource.buildApi(Api::class.java)
        return MainReposeitry(api)
    }

}
