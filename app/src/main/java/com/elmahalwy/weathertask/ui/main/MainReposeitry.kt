package com.elmahalwy.weathertask.ui.main

import com.elmahalwy.weathertask.base.BaseRepository
import com.elmahalwy.weathertask.base.CommonKeys
import com.elmahalwy.weathertask.network.Api
import com.elmahalwy.weathertask.network.RemoteDataSource

class MainReposeitry(private val api: Api) : BaseRepository() {
    suspend fun getCityInformation(searchText:String) =
        safeApiCall {
            api.getCityInformation(searchText,CommonKeys.APP_Id)
        }

}
