package com.elmahalwy.weathertask.network

import androidx.viewbinding.BuildConfig
import com.blankj.utilcode.util.NetworkUtils
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RemoteDataSource {
    companion object {
        const val BASE_URL = "http://api.openweathermap.org/"
    }

    fun <Api> buildApi(api: Class<Api>): Api {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .client(OkHttpClient.Builder()
                .addInterceptor { chain ->
                    chain.proceed(chain.request().newBuilder().also {
                    }.build())
                }.also { client ->
                    if (BuildConfig.DEBUG) {
                        val logging = HttpLoggingInterceptor()
                        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                        client.addInterceptor(logging)
                    }
                }.build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .client(getClient())
            .build().create(api)
    }

    private fun getClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.MINUTES)
            .readTimeout(2, TimeUnit.MINUTES)
            .addInterceptor(Interceptor { chain ->
                val original: Request = chain.request()
                val request: Request.Builder = original.newBuilder()
                request.header("Content-Type", "application/json")
                request.header("Accept", "application/json")
                    .method(original.method, original.body)
                if (!NetworkUtils.isConnected()) {
                    val cacheControl = CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build()
                    request.cacheControl(cacheControl)
                }
                chain.proceed(request.build())
            })
            .addNetworkInterceptor(Interceptor { chain ->
                val response: Response = chain.proceed(chain.request())
                if (NetworkUtils.isConnected()) {
                    val cacheControl = CacheControl.Builder()
                        .maxAge(1, TimeUnit.SECONDS)
                        .build()
                    response.newBuilder()
                        .header("Cache-Control", cacheControl.toString())
                        .build()
                } else {
                    // re-write response header to force use of cache
                    val cacheControl = CacheControl.Builder()
                        .maxAge(2, TimeUnit.MINUTES)
                        .build()
                    response.newBuilder()
                        .header("Cache-Control", cacheControl.toString())
                        .build()
                }
            }).addInterceptor(httpLoggingInterceptor)
            .build()
    }


}
