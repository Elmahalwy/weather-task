package com.elmahalwy.weathertask.network

import com.elmahalwy.weathertask.models.CityInformationResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface Api {
    // Auth //
    @GET("data/2.5/weather")
    suspend fun getCityInformation(
        @Query("q") searchText: String,
        @Query("appid") appId: String,
    ): CityInformationResponse


}
