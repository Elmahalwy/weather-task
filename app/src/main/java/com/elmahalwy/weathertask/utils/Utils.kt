package com.elmahalwy.weathertask.base


import android.app.Activity
import android.content.Intent
import java.text.SimpleDateFormat
import java.util.*


fun <A : Activity> Activity.startNewActivity(activity: Class<A>) {
    Intent(this, activity).also {
        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(it)
    }

}

fun convertKelivnToClieus(keliven: Double): Double {
    return keliven - 273.15
}

fun getCurrentDate(): String {
    val c = Calendar.getInstance().time
    println("Current time => $c")
    val df = SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault())
    return df.format(c)
}



