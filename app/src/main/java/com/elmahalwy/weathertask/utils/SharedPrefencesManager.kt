package com.elmahalwy.weathertask.utils

import android.content.Context
import android.content.SharedPreferences
import com.elmahalwy.weathertask.models.CityHistoricalModel

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class SharedPrefencesManager {
    companion object {
        const val SHARED_PREF_NAME = "weather_task"


        fun getCityHistorical(mContext: Context): List<CityHistoricalModel> {
            val prefs: SharedPreferences = mContext.getSharedPreferences(
                mContext.packageName, Context.MODE_PRIVATE
            )
            val gson = Gson()
            val json = prefs.getString("city_historical", "")
            val type: Type = object : TypeToken<List<CityHistoricalModel?>?>() {}.type
            val arrayList: List<CityHistoricalModel> = gson.fromJson<List<CityHistoricalModel>>(json, type)
            return arrayList

        }

        fun setCityHistorical(data: ArrayList<CityHistoricalModel>?, mContext: Context) {
            val editor: SharedPreferences.Editor = mContext.getSharedPreferences(
                mContext.packageName, Context.MODE_PRIVATE
            ).edit()
            val gson = Gson()

            val json = gson.toJson(data)
            editor.putString("city_historical", json)
            editor.apply()
        }


    }

}
