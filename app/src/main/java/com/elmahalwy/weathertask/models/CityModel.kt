package com.elmahalwy.weathertask.models

data class CityModel(
    val name: String,
    val lat: Double,
    val lng: Double
)
