package com.elmahalwy.weathertask.models

data class CityHistoricalModel(
    val cityName: String,
    val date: String,
    val weatherStatus: String,
)
