# Plan Reader Task

This project is a coding task by **Mohamed Elmahalwy**.   

# Brief For Task

- List all cities to user .
- user show current weather of city .
- user show historical data of search.


# Project usage tools

- Used kotlin programming language.
- Used kotlin Coroutine.
- Used Material design commpnetent in layouts.
- Used navagtion compentent and single activity.
- Making anaimtion when transtions between fragment .
- Used glide to load image .
_ Used viewbinding .


# Architecture
- Used clean architecture using Mvvm architecture pattern.






